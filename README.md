
* Ruby version - 2.4.1

* Rails version -  rails, 5.1.6

* psql (PostgreSQL) 10.5


How to run this project
1) clone project
git clone https://pranay05@bitbucket.org/pranay05/renovate.git

2) fetch all gem libraries
bundle install

3) create database
rake db:create

4) migrate database
rake db:migrate

5) run server
rails s


Summary:

Assumptions and notes:

1. There is scope for authetication/authorization, but for simplicity of project we dont have any user authetication/authorization right now.

2. The system does not implement any push notifications. However, the specific persona's will be able to access the pages and pull the information.

3. Though the backend code supports edit of contractor and project details but the UI interface links has been removed to avoid extra handling of use cases (updating recommendation) in demo project.

This project is basically divided into 2 modules.
For :
1) Contractor's module

2) Home owner's module


----------------
For 'Home Owner'
----------------

How it works?

a) Create your project

b) Meet your contractor's matches (the system will suggest top three contractors matching the requirements)

c) accept or reject the suggested contractors

d) accepted contractors will get notified and will be accepting or rejecting the recommended projects. Once a contractor accepts the project the deal gets done (technically it's a Match Found condition).

e) Happy renovating

----------------
For 'Contractors'
----------------

How it works?

a) See the curated list of projects where home owner's have chosen you.

b) accept or reject basis your preferences and with help of given data

c) Happy renovating the accepted projects


----------------
Demonstration steps:
----------------
1. Click on Switch persona in navbar and choose 'access as contractor'.
The switch persona feature in the demo project allows to access the web app as any one of the stakeholders (a homeowner or a contractor).

2. The landing page allows to create new contractor (aka contractor SIGN UP) or access using existing contractor id (aka LOG IN).

Create 5-7 contractors to set up the initial list of service providers.

3. Switch persona to 'Home Owner'

4. The landing page allows to create new project (aka HomeOwner Project SIGN UP) or access using existing project id (aka project LOG IN).

Create project with requirement details.

5. Upon successful creation, the system will generate max three recommended contractor suggestions with other details.
 Accept or Reject these recommendations basis your requirement.
 Note: if you reject all the suggestions then the system wont generate any new recommendations. The feature can be extended in future.

6. The accepted contractors will be able to see your project as  recommended projects on their dashboard.
Log In as specific contractor to see this in action.

7. Once the contractor also accepts the suggestion the match completes.



* Matching Algorithm
===================

1) The algorithm takes a project and returns top three recommended contractors basis a matching strategy.
The list of strategies can be further extended, the demo project assumed one matching strategy as given below.

Default Matching strategy:
1. Evaluate the list of eligible contractors (eligible_contractors[]). This simply finds all the contractors who provide the services required in a project.
Any contractor who is not providing the necessary services are eliminated out.

2) Score the eligible_contractors[] based on the budget of project and contractor' budget.

    # Score value 3:
    Best match. Contractor budget is subset of project budget. The project owner is most likely to accept this contractor.
    Eg: Project budget 100$ - 200$ and contractor budget 120$ - 160$

    # Score value 2:
    Project budget is subset of contractor budget.
    Eg: Project budget 100$ - 200$ and contractor budget 80$ - 700$

    # Score value 1:
    Some overlap in budgets.
    Eg: Project budget 100$ - 200$ and contractor budget 180$ - 700$

    # Score value 0:
    No overlap in budgets. Very rarely but if no matching contractors are their then project owners may choose contractors with score value of 0.
    Eg: Project budget 100$ - 200$ and contractor budget 500$ - 700$


    This is calculated by running a simple database query.
    This step returns contractors grouped by score 'contractors_by_budget_score'.

3) Takes the map contractors_by_budget_score and start creating the list of final_recommendations by taking contractors in decreasing order of score.

GEO-SPATIAL DISTANCE TO RESOLVE SCORE CONFLICTS:
For any 2 or more contractors having "same value of score give precedence to the one with lesser distance from the project location"

retuns the list of final contractors

4) return the top three recommendations.





