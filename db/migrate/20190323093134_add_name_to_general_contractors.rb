class AddNameToGeneralContractors < ActiveRecord::Migration[5.1]
  def change
    add_column :general_contractors, :name, :string
  end
end
