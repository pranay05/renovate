class CreateGeneralContractors < ActiveRecord::Migration[5.1]
  def change
    create_table :general_contractors do |t|
      t.decimal :latitude, null: false
      t.decimal :longitude, null: false
      t.integer :maximum_budget_willing, null: false
      t.integer :minimum_budget_willing, null: false
      t.boolean :design_services_offered
      t.boolean :build_services_offered

      t.timestamps
    end
  end
end
