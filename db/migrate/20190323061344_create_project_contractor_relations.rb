class CreateProjectContractorRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :project_contractor_relations do |t|
      t.integer :project_id
      t.integer :general_contractor_id
      t.boolean :project_owner_accepted
      t.boolean :contractor_accepted

      t.timestamps
    end
  end
end
