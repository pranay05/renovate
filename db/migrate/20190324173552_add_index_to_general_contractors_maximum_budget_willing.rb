class AddIndexToGeneralContractorsMaximumBudgetWilling < ActiveRecord::Migration[5.1]
  def change
    add_index :general_contractors, :maximum_budget_willing
    add_index :general_contractors, :minimum_budget_willing
  end
end
