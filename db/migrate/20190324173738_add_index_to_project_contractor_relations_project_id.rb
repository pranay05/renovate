class AddIndexToProjectContractorRelationsProjectId < ActiveRecord::Migration[5.1]
  def change
    add_index :project_contractor_relations, :project_id
    add_index :project_contractor_relations, :general_contractor_id
  end
end
