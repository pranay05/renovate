class AddColumnsToProjectContractorRelation < ActiveRecord::Migration[5.1]
  def change
    add_column :project_contractor_relations, :score, :integer
    add_column :project_contractor_relations, :distance, :decimal
  end
end
