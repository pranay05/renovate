class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.decimal :latitude, null: false
      t.decimal :longitude, null: false
      t.integer :maximum_budget, null: false
      t.integer :minimum_budget, null: false
      t.text :description
      t.boolean :design_services_needed
      t.boolean :build_services_needed

      t.timestamps
    end
  end
end
