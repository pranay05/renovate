class AddIndexToProjectsMaximumBudget < ActiveRecord::Migration[5.1]
  def change
    add_index :projects, [:maximum_budget, :minimum_budget]
  end
end
