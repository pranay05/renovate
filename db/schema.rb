# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190325054215) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contractors", force: :cascade do |t|
    t.decimal "latitude", null: false
    t.decimal "longitude", null: false
    t.integer "maximum_budget_willing", null: false
    t.integer "minimum_budget_willing", null: false
    t.boolean "design_services_offered"
    t.boolean "build_services_offered"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "general_contractors", force: :cascade do |t|
    t.decimal "latitude", null: false
    t.decimal "longitude", null: false
    t.integer "maximum_budget_willing", null: false
    t.integer "minimum_budget_willing", null: false
    t.boolean "design_services_offered"
    t.boolean "build_services_offered"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["maximum_budget_willing"], name: "index_general_contractors_on_maximum_budget_willing"
    t.index ["minimum_budget_willing"], name: "index_general_contractors_on_minimum_budget_willing"
  end

  create_table "project_contractor_relations", force: :cascade do |t|
    t.integer "project_id"
    t.integer "general_contractor_id"
    t.boolean "project_owner_accepted"
    t.boolean "contractor_accepted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "score"
    t.decimal "distance"
    t.index ["general_contractor_id"], name: "index_project_contractor_relations_on_general_contractor_id"
    t.index ["project_id"], name: "index_project_contractor_relations_on_project_id"
  end

  create_table "projects", force: :cascade do |t|
    t.decimal "latitude", null: false
    t.decimal "longitude", null: false
    t.integer "maximum_budget", null: false
    t.integer "minimum_budget", null: false
    t.text "description"
    t.boolean "design_services_needed"
    t.boolean "build_services_needed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["maximum_budget", "minimum_budget"], name: "index_projects_on_maximum_budget_and_minimum_budget"
  end

end
