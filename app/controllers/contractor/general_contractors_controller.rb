class Contractor::GeneralContractorsController < ApplicationController
  layout 'contractor_layout'

  before_action :set_general_contractor, only: [:show, :edit, :update]

  def index
    @general_contractors = GeneralContractor.all
    respond_to do |format|
      format.html  # index.html.erb
      format.json  { render :json => @general_contractors }
    end
  end

  def new
    @general_contractor = GeneralContractor.new
  end

  def create
    validate_create_request
    @general_contractor = GeneralContractor.new(contractor_params)
    @general_contractor.save!

    respond_to do |format|
      format.html { redirect_to contractor_general_contractor_path(@general_contractor), notice: 'Contractor created successfully.' }
      format.json  { render :json => @general_contractor }
    end
  rescue Exception => e
    subject = "Error: Exception in GeneralContractorsController#create. Msg => #{ e.message }."
    details =  params.inspect
    logger.error (subject + details)
    redirect_to new_contractor_general_contractor_path, :alert => e.message
  end

  def show
    @project_recommended =  @general_contractor.project_recommendations.where(project_owner_accepted: true)
  end

  def edit
  end

  def update
    validate_create_request
    respond_to do |format|
      if @general_contractor.update_attributes(contractor_params)
        format.html { redirect_to edit_contractor_general_contractor_path(@general_contractor), notice: 'Contractor was successfully updated.' }
      else
        format.html { render :edit }
      end
    end

  rescue Exception => e
    subject = "Error: Exception in GeneralContractorsController#update. Msg => #{ e.message }."
    details =  params.inspect
    logger.error (subject + details)

    redirect_to edit_contractor_general_contractor_path(@general_contractor), :alert => e.message
  end


  private

  def validate_create_request
    ExceptionUtils.check_if(params[:general_contractor][:maximum_budget_willing].to_i >= params[:general_contractor][:minimum_budget_willing].to_i,
                            'Maximum budget willing cannot be less than minimum budget willing.')

    ExceptionUtils.check_if(params[:general_contractor][:design_services_offered] == '1' || params[:general_contractor][:build_services_offered] == '1',
                            'At-least one service should be chosen for the project.')

  end

  def set_general_contractor
    @general_contractor = GeneralContractor.find(params[:id])
    raise Exception.new("Project with #{params[:id]} not found") if @general_contractor.nil?
  end

  def contractor_params
    params.require(:general_contractor).permit(:id ,:name,:latitude,:longitude, :maximum_budget_willing, :minimum_budget_willing, :design_services_offered,
                                    :build_services_offered
    )
  end


end
