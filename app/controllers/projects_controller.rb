class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update]

  def index
    @projects = Project.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @projects }
    end
  end

  def new
    @project = Project.new
  end

  def create
    validate_create_request
    @project = Project.new(project_params)
    @project.save!

    respond_to do |format|
      format.html { redirect_to project_path(@project), notice: 'Project was successfully created.' }
      format.json { render :json => @projects }
    end

  rescue Exception => e
    subject = "Error: Exception in project#create. Msg => #{ e.message }."
    details = params.inspect
    logger.error (subject + details)

    redirect_to new_project_path(@project), :alert => e.message
  end

  def show
    if @project.contractor_recommendations.blank?
      arr_of_top_contractors = RecommendationManagementService.get_top_three_contractors_for_project(@project.id)
      RecommendationManagementService.persist_recommendations(arr_of_top_contractors, @project.id)
    end

    @project.reload
    @recommendations = @project.contractor_recommendations
  end

  def edit
  end

  def update
    validate_create_request

    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to edit_project_path(@project), notice: 'Project was successfully updated.' }
        format.json { render :json => @projects }
      else
        format.html { render :edit }
      end
    end

  rescue Exception => e
    subject = "Error: Exception in project#update. Msg => #{ e.message }."
    details = params.inspect
    logger.error (subject + details)

    redirect_to edit_project_path(@project), :alert => e.message
  end

  # todo:
  def update_recommendation
    @recommendation_id = params[:id].split('_').last

    @recommendation = ProjectContractorRelation.find(@recommendation_id)
    value_to_update = ActiveModel::Type::Boolean.new.cast(params[:value])

    if params[:id].include? 'project_owner_accepted'
      @recommendation.update_column(:project_owner_accepted, value_to_update)
    elsif params[:id].include? 'contractor_accepted'
      @recommendation.update_column(:contractor_accepted, value_to_update)
    end
  end


  private

  def validate_create_request
    ExceptionUtils.check_if(params[:project][:maximum_budget].to_i >= params[:project][:minimum_budget].to_i,
                            'Maximum budget cannot be less than minimum budget.')

    ExceptionUtils.check_if(params[:project][:design_services_needed] == '1' || params[:project][:build_services_needed] == '1',
                            'At-least one service should be chosen for the project.')

  end

  def set_project
    @project = Project.find(params[:id])
    raise Exception.new("Project with #{params[:id]} not found") if @project.nil?
  end

  def project_params
    params.require(:project).permit(:id, :name, :latitude, :longitude, :maximum_budget, :minimum_budget, :description, :design_services_needed,
                                    :build_services_needed
    )
  end

end
