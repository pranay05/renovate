class RenovateServiceException < Exception
  attr_accessor :display_msg

  def initialize(msg)
    @display_msg = msg

    super(msg)
  end
end