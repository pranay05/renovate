class Project < ApplicationRecord
  has_many :contractor_recommendations, :class_name => "ProjectContractorRelation"
  has_many :general_contractors, through: :contractor_recommendations

  validates_presence_of :name, :longitude, :latitude, :maximum_budget, :minimum_budget
  validates_uniqueness_of :name #assuming name of project is unique handle of the project

end
