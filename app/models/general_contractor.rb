class GeneralContractor < ApplicationRecord
  has_many :project_recommendations, :class_name => "ProjectContractorRelation"
  has_many :projects, through: :project_recommendations

  validates_presence_of :name, :longitude, :latitude,:maximum_budget_willing, :minimum_budget_willing
  validates_uniqueness_of :name
  validates :maximum_budget_willing,:maximum_budget_willing , format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }
  #validate :range_limit

  private
  def range_limit
    if maximum_budget_willing < minimum_budget_willing
      errors.add(:maximum_budget_willing, "cannot be less than minimum budget willing")
    end
  end

end
