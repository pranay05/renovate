class ProjectContractorRelation < ApplicationRecord
  belongs_to  :project
  belongs_to :general_contractor

end
