class ExceptionUtils

  def self.check_if(condition, exception_message)
    if(!condition)
      raise RenovateServiceException.new(exception_message)
    end
  end

end