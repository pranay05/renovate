class RecommendationManagementService

  def self.get_top_three_contractors_for_project(project_id)
    MatchService.get_top_three_contractors_for_project(project_id)
  end

  def self.persist_recommendations(contractors_array, project_id)
    contractors_array.each do |contractor|
      ProjectContractorRelation.create(project_id: project_id, general_contractor_id: contractor["id"], score: contractor["score"], distance: contractor["distance_in_kms"])
    end
  end

end