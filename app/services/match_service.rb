class MatchService

  def self.get_top_three_contractors_for_project(project_id)
    @project = find_project(project_id)
    match_contractors[0..2]
  end


  private
  def self.match_contractors
    eligible_contractor_ids = get_eligible_contractors
    return [] if eligible_contractor_ids.blank?

    contractors_grouped_by_budget_score = score_by_budget(eligible_contractor_ids)

    get_final_list(contractors_grouped_by_budget_score)
  end

  def self.get_eligible_contractors
    eligible_contractor_ids = nil

    if @project.design_services_needed
      eligible_contractor_ids = design_services_offering_contractor

    elsif @project.build_services_needed
      eligible_contractor_ids = build_services_offering_contractor

    elsif @project.build_services_needed && @project.design_services_needed
      eligible_contractor_ids = both_services_contractors
    end
    eligible_contractor_ids
  end

  def self.find_project(project_id)
    Project.find(project_id)
  end

  def self.sort_by_distance_from_project(contractors)
    contractors.sort_by! { |element| element["distance_in_kms"] }
  end


  # Score: 3 Best match by budget.    Eg: Project budget 100$ - 200$ and contractor budget 120$ - 160$
  # Score: 2 Project budget is subset of contractor budget. Eg: Project budget 100$ - 200$ and contractor budget 80$ - 700$
  # Score: 1 Some overlap in budget.  Eg: Project budget 100$ - 200$ and contractor budget 180$ - 700$
  # Score: 0 No overlap in budget.    Eg: Project budget 100$ - 200$ and contractor budget 500$ - 700$
  def self.score_by_budget(contractor_ids)

    GeneralContractor.select("id, latitude, longitude, CASE WHEN minimum_budget_willing >= #{@project.minimum_budget} AND maximum_budget_willing <= #{@project.maximum_budget} THEN 3
                                    WHEN minimum_budget_willing < #{@project.minimum_budget} AND maximum_budget_willing > #{@project.maximum_budget} THEN 2
                                    WHEN (minimum_budget_willing BETWEEN #{@project.minimum_budget} AND #{@project.maximum_budget}) OR
                                          (maximum_budget_willing BETWEEN #{@project.minimum_budget} AND #{@project.maximum_budget}) THEN 1
                                    ELSE 0
                               END score")
                      .where(id: contractor_ids).to_a.group_by(&:score)
  end

  def self.get_final_list(contractors_grouped_by_score)
    final_list = []

    (0..3).to_a.reverse.each do |score|
      contractors_at_a_given_score = contractors_grouped_by_score[score]
      next if contractors_at_a_given_score.nil?

      contractors_data_as_json = get_contractors_with_distance(contractors_at_a_given_score)

      if contractors_data_as_json.size > 1 # resolve budget score clashes giving preference to lower distances from project location
        contractors_data_as_json = sort_by_distance_from_project(contractors_data_as_json)
      end

      final_list += contractors_data_as_json
    end
    final_list
  end

  def self.get_contractors_with_distance(contractors_at_a_given_score)
    contractors_data_as_json = []
    contractors_at_a_given_score.each do |contractor|
      contractor_data = contractor.as_json
      distance = LocationUtils.calculate_distance_in_meters([@project.latitude, @project.longitude], [contractor.latitude, contractor.longitude])
      contractor_data['distance_in_kms'] = (distance / 1000).round(1)
      contractors_data_as_json << contractor_data
    end
    contractors_data_as_json
  end

  def self.design_services_offering_contractor
    GeneralContractor.where(design_services_offered: true).pluck(:id)
  end

  def self.build_services_offering_contractor
    GeneralContractor.where(build_services_offered: true).pluck(:id)
  end

  def self.both_services_contractors
    GeneralContractor.where(build_services_offered: true, design_services_offered: true).pluck(:id)
  end

end


#todo index on entities

#todo if already generated contractor then dont , else get top 3 from match algois there,