Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "projects#index"

  resources :projects
  post '/update_recommendation' => 'projects#update_recommendation', :as => :update_recommendation

  namespace :contractor do
    resources :general_contractors
  end
end
